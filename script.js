const StaticText = {
  landing: {
    title: "¡Estás en la posición #150<br> para ser pionero bold!",
    landingText:
      "Para mantenerte dentro de los primeros 500 de la lista de espera, refiere a tus amigos y sube de puesto.",
    landingTextTwo:
      "Entre más anugis refieras, estarás más cerca de obtener tu datáfono a precio de pioneros bold:",
    landingTextPrice: "$99,000",
    landingTextThree:
      "Comparte este link con tus amigos y no te quedes por fuera de la lista:",
    landingLinkText: "Link único de referencia",
    landingLink: "https://links.bold.co/2019",
    landingButton: "Copiar",
    landingSocialButtons: [
      {
        title: "E-mail",
        color: "#DDDDDD",
        icon: "envelope"
      },
      {
        title: "Facebook",
        color: "#3b5998",
        icon: "facebook"
      },
      {
        title: "Twitter",
        color: "#1da1f2",
        icon: "twitter"
      },
      {
        title: "Whatsapp",
        color: "#25d366",
        icon: "whatsapp"
      }
    ]
  },
  leaderScore: {
    leaderscorePositionText: "Tu posición",
    leaderscorePositionNumber: "#150",
    leaderscoreTableColumns: ["Posición", "Nombre", "# Referidos"],
    people: [
      {
        email: "tkreynkrkpzqailsqw@ttirv.net",
        points: 20
      },
      {
        email: "9tata9afcomd@향로2.com",
        points: 19
      },
      {
        email: "nmohamed.aersa7@vfru80.info",
        points: 18
      },
      { email: "xkayl@향로2.com", points: 17 },
      {
        email: "dyusef.dramnam@7219oo.com",
        points: 16
      },
      {
        email: "laxutture-4131@yopmail.com",
        points: 15
      },
      {
        email: "ijudyhemm-5655@yopmail.com",
        points: 14
      },
      {
        email: "ippijimmu-2350@yopmail.com",
        points: 13
      },
      {
        email: "yvattori-6496@yopmail.com",
        points: 12
      },
      {
        email: "megemannemmo-6298@yopmail.com",
        points: 11
      },
      {
        email: "enixami-3225@yopmail.com",
        points: 10
      },
      {
        email: "affifaddip-0140@yopmail.com",
        points: 9
      },
      {
        email: "akinneddurro-6169@yopmail.com",
        points: 8
      },
      {
        email: "eparrenu-7266@yopmail.com",
        points: 7
      },
      {
        email: "udytahu-6741@yopmail.com",
        points: 6
      }
    ]
  }
};
const Elements = {
  landing: {
    title: document.querySelector('[data-js="landingTitle"]'),
    landingText: {
      textOne: document.querySelector('[data-js="landingText-one"]'),
      textTwo: document.querySelector('[data-js="landingText-two"]'),
      textThree: document.querySelector('[data-js="landingText-three"]')
    },
    landingSocial: {
      title: document.querySelector('[data-js="landingSocial-title"]'),
      link: document.querySelector('[data-js="landignSocial-link"]'),
      button: document.querySelector('[data-js="landingSocial-button"]'),
      buttonGroup: document.querySelector(
        '[data-js="landingSocial-buttonGroup"]'
      )
    }
  },
  leaderScore: {
    myPosition: document.querySelector('[data-js="leaderscore-myPosition"]'),
    topPeople: document.querySelector('[data-js="leaderscore-topPeople"]'),
    peopleHeader: document.querySelector(
      '[data-js="leaderscore-peopleHeader"]'
    ),
    people: document.querySelector('[data-js="leaderscore-people"]')
  }
};

//Constants

const leaderSize = 3;

//Pre-render
const ButtonGroupHTML = StaticText.landing.landingSocialButtons.map(button =>
  button.title === "E-mail"
    ? ` <button style="background-color: ${button.color}; color:black">
          <i class="far fa-${button.icon}"></i>
          <span>${button.title}</span>
        </button>`
    : ` <button style="background-color: ${button.color}">
          <i class="fab fa-${button.icon}"></i>
          <span>${button.title}</span>
        </button>`
);

const MyPositionHTML = `${StaticText.leaderScore.leaderscorePositionText} <span>${StaticText.leaderScore.leaderscorePositionNumber}</span>`;

const TopLeadersHTML = StaticText.leaderScore.people.slice(0, leaderSize).map(
  (item, index) =>
    `
  <div class="leader">
    <div class="circle">
      <img src="./assets/user.svg" alt="" />
    </div>
    <div class="position-number">
      <span>${index == 1 ? 1 : index + 2}</span>
    </div>
    <div class="position-number-flag"><img src="./assets/price.svg"/></div>
    <div class="email-text">${item.email}</div>
  </div>

`
);
const PeopleHeaderHTML = StaticText.leaderScore.leaderscoreTableColumns.map(
  item => `<span>${item}</span>`
);

let PeopleHTML = StaticText.leaderScore.people.map(
  (item, index) => `
<div class="person">
  <span class="position">${index + 2}</span>
  <span class="name">
    ${item.email}
  </span>
  <span class="score">${item.points}</span>
</div>`
);
console.log(PeopleHTML);
PeopleHTML.push(`<div class="person-showmore">
 <span class="name">
   Ver mas
 </span>
 </div>`);
//Landing
Elements.landing.title.innerHTML = StaticText.landing.title;
Elements.landing.landingText.textOne.innerHTML = StaticText.landing.landingText;
Elements.landing.landingText.textTwo.innerHTML = `${StaticText.landing.landingTextTwo} <b>${StaticText.landing.landingTextPrice}</b>`;
Elements.landing.landingText.textThree.innerHTML =
  StaticText.landing.landingTextThree;
console.log(Elements.landing.landingSocial.title);
Elements.landing.landingSocial.title.innerHTML =
  StaticText.landing.landingLinkText;
Elements.landing.landingSocial.link.innerHTML = StaticText.landing.landingLink;
Elements.landing.landingSocial.button.innerHTML =
  StaticText.landing.landingButton;
// console.log(Elements.landing.landingSocial);
Elements.landing.landingSocial.buttonGroup.innerHTML = ButtonGroupHTML;
Elements.landing.landingSocial.buttonGroup.innerHTML = ButtonGroupHTML.join("");

console.log(Elements.leaderScore);
//LeaderScore
Elements.leaderScore.myPosition.innerHTML = MyPositionHTML;
Elements.leaderScore.topPeople.innerHTML = TopLeadersHTML;
Elements.leaderScore.peopleHeader.innerHTML = PeopleHeaderHTML;
Elements.leaderScore.peopleHeader.innerHTML = PeopleHeaderHTML.join(" ");
Elements.leaderScore.people.innerHTML = PeopleHTML;
Elements.leaderScore.people.innerHTML = PeopleHTML.join(" ");
